igwn-alert Users Guide
======================

The International Gravitational Wave Network Alert System (igwn-alert) 
is a notification 
service built on the `Apache Kafka`_ protocol and the pubsub extension. 
It provides a basic notification tool which allows multiple producers 
and consumers of notifications. 

The igwn-alert client code a version of the generalized Kakfa client code,
`hop-client`_. It has been modified to streamline ingestion of alerts 
from GraceDB_, and to provide continuity with the previous-generation 
`LVAlert`_ service and client code, which made use use of the XMPP_ protocol.

The server backend infrastructure is built and maintained by SCiMMA_, as
is the authentication and identity access management. 

First Steps
-----------

To begin to use the service, you must first create an account on the 
`SCiMMA Auth`_ account management portal (https://my.hop.scimma.org/).

Create an account using LIGO, Virgo, or KAGRA credentials by choosing 
one of the respective organizations as your identity provider. Note:
at this time, access to igwn-alert is limited to LIGO, Virgo, 
and KAGRA members.

.. image:: _static/login_screen.png
  :width: 450
  :alt: SCiMMA Identity Providers. 

Next, you must join the :code:`lvk-users` group to subscribe to topics from GraceDB.
Please submit your  request as a new issue on the 
igwn-alert Gitlab project page by following `this link`_. Be sure to 
include your newly-created SCiMMA ID (SCiMMA1000XXXX), as well as your name
and email address that you used to register with SCiMMA. By authenticating 
through Gitlab, you are confirming that you have valid LVK credentials, and are
authorized to receive alerts from GraceDB via membership in the :code:`lvk-users` group. 
Within a short period of time, you will receive a confirmation through Gitlab
that your request was approved, and you will see the change 
reflected on the Auth account management screen.

To repeat, before creating credentials and subscribing to topics, please fill out 
the `request form <https://git.ligo.org/computing/igwn-alert/client/-/issues/new?issue%5Bmilestone_id%5D=#>`_
to be added to the :code:`lvk-users` group.


Managing Credentials and Topics
-------------------------------

Unlike the XMPP code, subscriptions to topics is handled by a **credential**,
which is managed through the `SCiMMA Auth`_ web portal.  Users can have 
multiple credentials associated with their account, each with their unique
credential names and passwords.

To create a credential, use the following prompt in the web interface:

.. image:: _static/create_credentials.png
  :width: 350
  :alt: Prompt to create a credential.

Record the password and credential name, as there is no ability to change
or recall passwords at this time. 

Subscriptions to topics are managed through credential **permissions**. Adding 
a "Read" permission is analogous to subscribing to receiving alerts. The
:code:`lvk-users` group has read-only access to topics from GraceDB. Topic names
are of the format: :code:`group.topic`, where :code:`group` refers to which 
instance of GraceDB to which
the user's process is listening (:code:`gracedb`, :code:`gracedb-playground`,
:code:`gracedb-test`). And the :code:`topic` follows the existing 
:code:`group_pipeline_search` schema. 

Note, adding the permission to read 
a topic is a required step to receive alerts from the topic. A topic will
not appear in the :code:`client.get_topics()` method if the read permission
has not been added. Attempting to 
listen or publish to a topic whose permission has not been added will return 
an error.

Adding Authentication
---------------------

At the current time, authentication for the igwn-alert client code is added
using the tools provided with `hop-client`_. Please refer to the `hop-client 
authentication guide`_ for more information. The easiest way to start to is 
to run the following command::

    hop auth add

And enter the username and password provided when you create a credential. 
By default, the authentication credentials are stored in a file, 
:code:`~/.config/hop/auth.toml`. 

Other means of authentication include support for :code:`.netrc` and inputting 
a credential's name and password directly when instantiating a
:code:`igwn_alert.client` class.

igwn-alert How To
-----------------

igwn-alert uses the Publish-Subscribe (PubSub) model to create and distribute 
alerts to users. An entity (most commonly, GraceDB_) publishes an alert
to a topic (think of it like a channel). Other entities subscribe to that topic
(channel) and then listen for content published on the channel. 

Alerts from GraceDB take the form of JSON_-formatted strings, whose contents 
depend on the action from GraceDB (e.g.: new event upload, new label applied, 
etc.). A description of  :code:`igwn-alert` message contents from GraceDB is available for 
events_ and superevents_.

Note that GraceDB sends alerts to topics according to the Group, Pipeline, 
and Search of the candidate event in question. The topic name is constructed 
by lower-casing each element and joining with underscores. Thus, an alert for 
an event from the CBC group, gstlal pipeline, and 'LowMass' search would be 
sent out over the topic ``cbc_gstlal_lowmass``. The Search element at the end is 
optional (i.e., the same alert will also be sent to the ``cbc_gstlal`` topic). 

The listener can be configured to take an action upon receipt of an alert.

Topics and Read/Write Permissions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

The topics above are designated to publish alerts from GraceDB **only**. This 
is a conscious design choice to avoid scenarios where unauthorized users 
mimic alerts from GraceDB and misdirect follow-up processes and observers. As such,
members of the :code:`lvk-users` group on SCIMMA only have **read** access to these 
topics. Attempting to publish to these topics by a non-GraceDB credential will
result in an error.

Responding to igwn-alert Messages
---------------------------------

The command-line tools, described on the `main page`_ allow users to quickly 
and easily interact wih the ``igwn-alert`` service, such as subscribing to and listing topics,
listening and displaying alerts via ``stdout``, etc. 

The API tools allow users to specify actions to be taken upon receipt of an
igwn-alert message. The action can be dependent on the topic which issues the message, as
well as the type and contents of the message.

Please see `the following example`_ in the igwn-alert gitlab repository 
as a place to start to write your own igwn-alert listener. The first block 
of code allows the user to hard-code the value of the server, username, and topics 
to use to interact with the service. The comments should be self-explanatory. 

The relevant block to respond to alerts begins in the `process_alert`_ 
callback function. This function is called when an alert comes in, and returns a 
string value ``topic``, which is the name of the topic from which the message 
was received, and a JSON packet that contains the alert contents. Note, the contents 
of an alert for different alert types can be found on the GraceDB documentation 
linked previously on this page. In this block, users can call any imported Python 
module to take a unique action upon receiving an alert. 

Please contact `computing-help@igwn.org`_ with further questions. 

.. _Apache Kafka: https://kafka.apache.org/
.. _hop-client: https://hop-client.readthedocs.io/
.. _GraceDB: https://gracedb.ligo.org/
.. _LVAlert: https://lscsoft.docs.ligo.org/lvalert
.. _SCiMMA: https://scimma.org/
.. _XMPP: https://xmpp.org/
.. _SCiMMA Auth: https://my.hop.scimma.org/
.. _computing-help@igwn.org: mailto:computing-help@igwn.org
.. _hop-client authentication guide: https://hop-client.readthedocs.io/en/latest/user/auth.html
.. _JSON: https://www.json.org/
.. _events: https://gracedb.ligo.org/documentation/models.html#base-event-model
.. _superevents: https://gracedb.ligo.org/documentation/models.html#superevents
.. _main page: index.html
.. _the following example: https://git.ligo.org/lscsoft/igwn-alert/-/blob/main/share/igwn_alert_listener
.. _process_alert: https://git.ligo.org/lscsoft/igwn-alert/-/blob/main/share/igwn_alert_listener#L78
.. _this link: https://git.ligo.org/computing/igwn-alert/client/-/issues/new?issue%5Bmilestone_id%5D=#
