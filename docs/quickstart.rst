Getting Started
===============

The latest version of igwn-alert is available in the `igwn-py39`_,
`igwn-py310`_, and `igwn-py311`_ conda environments.

The latest version is also available on pip_::

    pip install igwn-alert

For custom conda environments where `conda-forge`_ is enabled::

    conda install igwn-alert

Next, see the `User Guide on topics and credentials`_ for instructions on how to
enable your account and credentials.

Finally, listen for messages::

    igwn-alert -g gracedb-playground listen

.. _pip: https://pypi.org/project/igwn-alert/
.. _conda-forge: https://github.com/conda-forge/igwn-alert-feedstock
.. _User Guide on topics and credentials: guide.html#managing-credentials-and-topics
.. _igwn-py39: https://computing.docs.ligo.org/conda/environments/igwn-py39/
.. _igwn-py310: https://computing.docs.ligo.org/conda/environments/igwn-py39/
.. _igwn-py311: https://computing.docs.ligo.org/conda/environments/igwn-py39/
